#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include "fibonacci.h"
#include "generate.h"
#include <fstream>
#include <stdio.h>
using namespace std;


int main()
{
	const int M = 103;
	int S[M] = {}, SS[M][M] = {};

	Gen generate;
	int i, j, t1, t2, max1 = 0, temp, p = 0;
	for (i = 0; i < M; i++)
		for (j = 0; j < M; j++);
		while (p++ < 5000)
		{
			t1 = generate.fibonacci_random(M);
			//t2 = generate.fibonacci_random(M);
			S[t1]++;
			max1 = (max1 < S[t1]) ? S[t1] : max1;
			//SS[t1][t2]++;
		}
	cout << "Choose type of output, please :" << endl << "1 - console, 2 - html, 3 - picture" << endl;
	int in;
	cin >> in;
	if (in == 1)
	{
		temp = max1;
		for (j = 0; j < max1; j++)
		{
			for (i = 0; i < M; i++)
			{
				if (S[i] >= temp) cout << "*|"; else cout << " |";
			}
			cout << endl;
			temp--;
		}
	}
	else if (in == 2)
	{
		FILE* myfile = fopen("test.html", "w+");
		//fprintf(myfile, "<\n");
		fprintf(myfile, "<html>\n");
		fprintf(myfile, "<head>\n");
		fprintf(myfile, "</head>\n");
		fprintf(myfile, "<body>\n");
		fprintf(myfile, "<table>\n");
		//--------------
		temp = max1;
		for (j = 0; j < max1; j++)
		{
			fprintf(myfile, "<tr>\n");
			for (i = 0; i < M; i++)
			{
				if (S[i] >= temp) fprintf(myfile, "<td style=\"background :#999; width: 5px; height: 5px;\"> </td>"); else fprintf(myfile, "<td style=\"background :#BBB\"> </td>");
			}
			temp--;
			fprintf(myfile, "</tr>\n");
		}
		//--------------
		fprintf(myfile, "</table>\n");
		fprintf(myfile, "</body>\n");
		fprintf(myfile, "</html>\n");
		fclose(myfile);
		cout << "Look for test.html in project directory." << endl;
	}
	system("pause");
	return 0;
}