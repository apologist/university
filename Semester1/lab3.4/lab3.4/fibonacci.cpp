#include "fibonacci.h"
Fib::Fib(){
	F0 = 0;
	F1 = 1;
}

int Fib::fib(){
	F1 = F0 + F1;
	F0 = F1 - F0;
	return(F0);
}

Fib::Fib(int f0, int f1){
	F0 = f0;
	F1 = f1;
}
