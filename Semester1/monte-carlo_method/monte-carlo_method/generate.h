#include "fibonacci.h"

#ifndef _GENERATE_H
#define _GENERATE_H

#include <iostream>
#include "fibonacci.h"


class Gen{
private:
	Fib f;
	int MAX;

public:
	int fibonacci_random();
	float float_random();
	Gen();
	Gen(int);
};


#endif // _GENERATE_H