#include <iostream>
#include "figure.h"

using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");
	int length_square; // ������� ��������.
	int length_general; // ������� �������������� ������.
	int quantityOfPoints; // ����� ���������� �����.
	double squareFinal; // ������� ������� ������.
	int happyPoints = 0; // �����, ������������� ������.
	cout << "������� ����� ������� �������� : ";
	cin >> length_square;
	Figure myFigure(length_square); // ������� �������.
	double squareRight = 8 * ((sqrt(2)*length_square - length_square) / 2) * ((sqrt(2)*length_square - length_square) / 2); // ������ �������� ������� ������.
	do
	{
		cout << "������� ����� �������������� ������ (������ ����� ������� ��������) : ";
		cin >> length_general;
	} while (length_general < sqrt(2)*length_square + 1);
	do
	{
		cout << "������� ���������� ������������ ����� (������� 100) : ";
		cin >> quantityOfPoints;
	} while (quantityOfPoints < 100);
	squareFinal = myFigure.monteCarloTest(length_general, quantityOfPoints, happyPoints); 
	cout << "N = " << quantityOfPoints << endl << "N1 = " << happyPoints << endl;
	cout << "S = " << length_general*length_general << endl << "S1 = " << squareFinal << " (�������� �������� : " << squareRight << ")" << endl;
	system("pause");
	return 0;
}