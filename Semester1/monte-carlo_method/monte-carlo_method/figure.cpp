#include "figure.h"
#include "additional.h"
using namespace std;

// ����������, �� �������� ����� ����� �����.
int Figure::check(float x, float y){
	double lt = length_square * sqrt(2);
	double t1 = (lt - length_square) / 2;

	// �������� ��� ������.
	if (x > lt) return false;

	if (x > (lt - t1))
	{
		if (y >= f_down_right(x, lt) && y <= f_up_left(x, lt)) return true; else return false;
	}

	if (x == (lt - t1))
	{
		if (y >= t1 && y <= (lt - t1)) return true; else return false;
	}

	if (x >= (lt / 2 + t1))
	{
		if ((y <= f_down_right(x, lt) && y >= t1) || (y >= f_up_left(x, lt) && y <= (lt - t1))) return true; else return false;
	}

	if (x > (lt / 2 - t1))
	{
		if ((y <= t1 && y >= f_down_left(x, lt)) || (y >= (lt - t1) && y <= f_up_left(x, lt))) return true; else return false;
	}

	if (x > t1)
	{
		if ((y >= t1 && y <= f_down_left(x, lt)) || (y >= f_up_right(x, lt) && y <= (lt - t1))) return true; else return false;
	}

	if (x == t1)
	{
		if (y >= t1 && y <= (lt - t1)) return true; else return false;
	}

	if (x < t1)
	{
		if (y >= f_down_left(x, lt) && y <= f_up_right(x, lt)) return true; else return false;
	}
}

Figure::Figure(int len){
	length_square = len;
}
Figure::Figure(){
	length_square = 0;
}
double Figure::monteCarloTest(int len_general, int N, int &N1){
	Gen gen(len_general);
	float x = 0, y = 0;
	for (int i = 0; i <= N; i++)
	{
		// �������� �������� ����� ������� ��������� ��� ������ �-��� rand()
		x = gen.float_random();
		y = gen.float_random();
		if (check(x, y)) N1++;
	}
	int square_general = len_general*len_general; // ������� �������������� ������.
	return double(square_general*N1 /N);
}
