#include "generate.h"
#include "fibonacci.h"

int Gen::fibonacci_random(){
	return f.fib() % MAX;
}
float Gen::float_random(){
	return (static_cast <float> (rand()) / (static_cast <float> (RAND_MAX /MAX)));
}
Gen::Gen(){
	MAX = 10;
}
Gen::Gen(int m){
	MAX = m;
}
