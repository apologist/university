#ifndef _FIGURE_H
#define _FIGURE_H

#include "generate.h"
class Figure{
private:
	int length_square;
public:
	int check(float, float);
	double monteCarloTest(int, int, int&); // ���������� ������� ������
	Figure(int);
	Figure();
};

#endif // _FIGURE_H