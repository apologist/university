#include "generate.h"
//#include "fibonacci.h"

int Gen::fibonacci_random(){
	return abs(f.fib() % MAX);
}
float Gen::float_random(){
	return (static_cast <float> (rand()) / (static_cast <float> (RAND_MAX /MAX)));
}
int Gen::fibonacci_random(int m){
	return abs(f.fib() % m);
}
float Gen::float_random(int m){
	return (static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / m)));
}
Gen::Gen(){
	MAX = 10;
}
Gen::Gen(int m){
	MAX = m;
}

