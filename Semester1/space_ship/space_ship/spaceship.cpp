#include "spaceship.h"
#include <minmax.h>
SpaceShip::SpaceShip()
{
	doorHeight = 100;
	doorWidth = 40;
	generalEnergy = 10000000;
	generalVolume = 100000;
	generalWeight = 14000000000;
	minDensity = 12850;
}
SpaceShip::SpaceShip(int h , int w, int energy, int volume, int weight, int destiny)
{
	doorHeight = h;
	doorWidth = w;
	generalEnergy = energy;
	generalVolume = volume;
	generalWeight = weight;
	minDensity = destiny;
}
#pragma region GetSetEnergy

void SpaceShip::setGeneralEnergy(int energy)
{
	if (energy >= 0) generalEnergy = energy;
}
void SpaceShip::setGeneralVolume(int volume)
{
	generalVolume = volume;
}
void SpaceShip::setGeneralWeight(long long weight)
{
	generalWeight = weight;
}
void SpaceShip::setMinDensity(int density)
{
	minDensity = density;
}
int SpaceShip::getGeneralEnergy()
{
	return generalEnergy;
}
int SpaceShip::getGeneralVolume()
{
	return generalVolume;
}
long long SpaceShip::getGeneralWeight()
{
	return generalWeight;
}
int SpaceShip::getMinDensity()
{
	return minDensity;
}
double SpaceShip::getGoldVolume()
{
	return goldVolume;
}
double SpaceShip::getGoldWeight()
{
	return goldWeight;
}
void SpaceShip::setEnergyAccept(int energy)
{
	if (energy >= 0) 
		energyAccept = energy;
}
void SpaceShip::setEnergyTurn(int energy)
{
	if (energy >= 0) 
		energyTurn = energy;
}
void SpaceShip::setEnergyRotate(int energy)
{
	if (energy >= 0) 
		energyRotate = energy;
}
void SpaceShip::setEnergyDrop(int energy)
{
	if (energy >= 0) 
		energyDrop = energy;
}
void SpaceShip::setEnergyCut(int energy)
{
	if (energy >= 0) 
		energyCut = energy;
}
void SpaceShip::setEnergyPut(int energy)
{
	if (energy >= 0) 
		energyPut = energy;
}
int SpaceShip::getEnergyAccept()
{
	return energyAccept;
}
int SpaceShip::getEnergyTurn()
{
	return energyTurn;
}
int SpaceShip::getEnergyRotate()
{
	return energyRotate;
}
int SpaceShip::getEnergyDrop()
{
	return energyDrop;
}
int SpaceShip::getEnergyCut()
{
	return energyCut;
}
int SpaceShip::getEnergyPut()
{
	return energyPut;
}
int SpaceShip::getDoorHeight()
{
	return doorHeight;
}
int SpaceShip::getDoorWidth()
{
	return doorWidth;
}
void SpaceShip::setDoorHeight(int h)
{
	doorHeight = h;
}
void SpaceShip::setDoorWidth(int w)
{
	doorWidth = w;
}

#pragma endregion

#pragma region Operations

void SpaceShip::Put(GoldBar &bar)
{
	int h = bar.getHeight();
	int w = bar.getWidth();
	int l = bar.getLenght();
	int H = getDoorHeight();
	int W = getDoorWidth();
	if (!(bar.getDensity() >= minDensity))
	{
		Drop(bar);
		return;
	}
	if (((h > H && w > W) && (h > W && w > H)) ||
		((w > H && l > W) && (w > W && l > H)) ||
		((h > H && l > W) && (h > W && l > H)))
	{
		Drop(bar);
		return;
	}
	else
		if (h <= H && w <= W) Accept(bar); else
	{
		if ((h > H && w > W) && (h <= W && w <= H))
		{
			Rotate(bar);
			Accept(bar);
		} else
			if ((h > H && w <= W) && (l <= H))
			{
				Turn(bar);
				Accept(bar);
			} else
				if (w <= H && l <= W)
				{
				Turn(bar);
				Rotate(bar);
				Accept(bar);
				} else
					if (l <= H && h <= W)
					{
					Rotate(bar);
					Turn(bar);
					Accept(bar);
					}
					else
						if (h <= H && l <= W)
						{
					Rotate(bar);
					Turn(bar);
					Rotate(bar);
					Accept(bar);
						}
						else Drop(bar);

	}
	

}
void SpaceShip::Accept(GoldBar &bar)
{
	if (generalEnergy >= energyAccept)
		generalEnergy -= getEnergyAccept(); 
	else 
		return;
	if (bar.getHeight() <= doorHeight && bar.getWidth() <= doorWidth)
	{
		if (
			(goldVolume + (bar.getHeight() * bar.getLenght() * bar.getWidth())/1000000 < generalVolume) && 
			(goldWeight + (bar.getHeight() * bar.getLenght() * bar.getWidth()) / 1000000 < generalWeight)
		) {
			gold.push_back(bar);
			goldVolume += double(bar.getHeight() * bar.getLenght() * bar.getWidth()) / 1000000;
			goldWeight += (double(bar.getHeight() * bar.getLenght() * bar.getWidth()) / 1000000) * bar.getDensity();
		}
		else 
			return;
	} 
	else 
		errorsAlgo++;
}
void SpaceShip::Rotate(GoldBar &bar)
{
	if (generalEnergy >= energyRotate)
	{
		generalEnergy -= getEnergyRotate();
		int temp = bar.getHeight();
		bar.setHeight(bar.getWidth());
		bar.setWidth(temp);
	}
}
void SpaceShip::Turn(GoldBar &bar)
{
	if (generalEnergy >= energyTurn)
	{
		generalEnergy -= getEnergyTurn();
		int temp = bar.getHeight();
		bar.setHeight(bar.getLenght());
		bar.setLenght(temp);
	}
}
void SpaceShip::Drop(GoldBar &bar)
{
	if (generalEnergy >= energyDrop)
	{
		generalEnergy -= getEnergyDrop();
		droped++;
    }
else return;
}
void SpaceShip::Cut(GoldBar &bar)
{
	if (generalEnergy >= energyCut)
	{
		generalEnergy -= getEnergyCut();
		int l = (getGeneralVolume() - getGoldVolume()) / (bar.getHeight() * bar.getWidth());
		
	}
}

#pragma endregion

