#include <iostream>
#include "generate.h"
#include "spaceship.h"
using namespace std;

int main()
{
	/*
	const int M = 29;
	int S[M], SS[M][M];
	
	
	Gen generate(M);
	int i, j,t1,t2;
	for (i = 0; i < M; S[i++] = 0)
		for (j = 0; j < M; SS[i][j++] = 0);
	while (i++ < 100000){
		t1 = generate.fibonacci_random();
		t2 = generate.fibonacci_random();
		S[t1]++;
		SS[t1][t2]++;
	}
	*/
	SpaceShip ship;
	GoldBar tempGold;
	Gen generate;
	int R = 140;
	int temp = 0;
	int maxGoldDensity = 19300;
	int minGoldDensity = 11240;
	ship.setDoorHeight(100);
	ship.setDoorWidth(40);
	ship.setMinDensity(12850);
	ship.setGeneralEnergy(10000000);
	while (
		ship.getGeneralEnergy() > ship.getEnergyPut() && 
		ship.getGeneralVolume() > ship.getGoldVolume() && 
		ship.getGeneralWeight() > ship.getGoldWeight()
	) {
		
		tempGold.setHeight(generate.fibonacci_random(R-1)+1);
		tempGold.setWidth(generate.fibonacci_random(R-1)+1);
		tempGold.setLenght(generate.fibonacci_random(R-1)+1);
		tempGold.setDensity(generate.fibonacci_random(maxGoldDensity-minGoldDensity)+minGoldDensity);
		int q = ship.gold.size();
		ship.Put(tempGold);
		if (q == ship.gold.size()) 
			temp++; 
		else 
			temp = 0;
		if (temp > 1000) 
			break;
	}

	cout << "Number of bars : " << ship.gold.size() << endl;
	cout << "Volume : "  << ship.getGoldVolume() << "/" << ship.getGeneralVolume() << endl;
	cout << "Weight : " << fixed << cout.precision(4) << ship.getGoldWeight() << "/" << ship.getGeneralWeight() << endl;
	cout << "Energy : " << ship.getGeneralEnergy() << endl;
	cout << "Droped : " << ship.droped << endl;
	cout << "Number of errors : " << ship.errorsAlgo << endl;
	system("pause");
	return 0;
}