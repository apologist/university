#ifndef _SPACESHIP_H
#define _SPACESHIP_H

#include "goldbar.h"
#include <vector>

class SpaceShip
{
private:
	int doorWidth; // sm
	int doorHeight; // sm
	int energyTurn = 15;
	int energyRotate = 15; 
	int energyAccept = 10;
	int energyDrop = 10;
	int energyCut =20;
	int energyPut = 10;
	int generalEnergy;
	int generalVolume;
	long long generalWeight;
	int minDensity = 11240;
	double goldWeight = 0;
	double goldVolume = 0;
public:
	int droped = 0;
	SpaceShip();
	SpaceShip(int, int, int, int, int, int);
	void setGeneralEnergy(int);
	void setGeneralVolume(int);
	void setGeneralWeight(long long);
	void setMinDensity(int);
	int getGeneralEnergy();
	int getGeneralVolume();
	long long getGeneralWeight();
	int getMinDensity();	
	double getGoldWeight();
	double getGoldVolume();
	void setEnergyTurn(int);
	void setEnergyRotate(int);
	void setEnergyAccept(int);
	void setEnergyDrop(int);
	void setEnergyCut(int);
	void setEnergyPut(int);
	int getEnergyTurn();
	int getEnergyRotate();
	int getEnergyAccept();
	int getEnergyDrop();
	int getEnergyCut();
	int getEnergyPut();
	void Turn(GoldBar&);
	void Rotate(GoldBar&);
	void Accept(GoldBar&);
	void Drop(GoldBar&);
	void Cut(GoldBar&);
	void Put(GoldBar&);
	int getDoorWidth();
	int getDoorHeight();
	void setDoorWidth(int);
	void setDoorHeight(int);
	std::vector <GoldBar> gold;
	int errorsAlgo;
};
#endif 