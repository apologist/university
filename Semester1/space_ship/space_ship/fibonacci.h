#ifndef _FIBONACCI_H
#define _FIBONACCI_H

class Fib{

private:
	int F0;
	int F1;
public:
	Fib();
	int fib();
	int fib(int);
	Fib(int, int);
};

#endif // _FIBONACCI_H