#ifndef _GENERATE_H
#define _GENERATE_H

#include "fibonacci.h"
#include <iostream>

class Gen{
private:
	Fib f;
	int MAX;

public:
	int fibonacci_random();
	float float_random();
	int fibonacci_random(int);
	float float_random(int);
	Gen();
	Gen(int);
};


#endif // _GENERATE_H