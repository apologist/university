#ifndef _GOLDBAR_H
#define _GOLDBAR_H

class GoldBar
{
private:
	int width;
	int height;
	int lenght;
	int density;
public:
	GoldBar();
	GoldBar(int, int, int,int);
	int getWidth();
	int getHeight();
	int getLenght();
	int getDensity();
	void setWidth(int);
	void setHeight(int);
	void setLenght(int);
	void setDensity(int);
};
#endif 