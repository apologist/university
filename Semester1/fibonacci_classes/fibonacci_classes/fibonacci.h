#ifndef _FIBONACCI_H
#define _FIBONACCI_H

class Fib{

private:
	int F0;
	int F1;
public:
	Fib();
	int fib();
	int initfib(int);
	Fib(int, int);
	int getF0();
	int getF1();
};

#endif // _FIBONACCI_H