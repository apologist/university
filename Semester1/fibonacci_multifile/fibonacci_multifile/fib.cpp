#include "fib.h";
static int F0 = 0;
static int F1 = 1;
void initfibonacci(int n, int &f0, int &f1)
{
	while (n--)
	{
		f0 = (f1 = f0 + f1) - f0;
	}
}
int fib()
{
	return F0 = (F1 = F0 + F1) - F0;
}