﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;


namespace IncubatorCS
{
    class Animal
    {
        const int minAge = 0;
        const int maxAge = 30;
        const int feedNorm = 10;
        const int waterNorm = 10;
        const int maxHealth = 100;
        const int time = 5000;
        int health;
        int age;
        int ageDeath;
        Farm hostFarm = Farm.Instance;
        Random random = new Random();
        public Animal()
        {
            age = 0;
            ageDeath = random.Next(maxAge);
            health = maxHealth;
            LiveLife();
        }
        private async void LiveLife()
        {
            await DoWork();

        }
        private Task DoWork()
        {
            return Task.Run(() =>
            {
                while(health > 0 && age < ageDeath)
                {
                    eat();
                    drink();
                    ageUp();
                    Thread.Sleep(time);
                }
                die();
            });
        }
        private void eat()
        {
            if (hostFarm.getFeed(feedNorm) != feedNorm)
                health -= maxHealth / 4;
        }
        private void drink()
        {
            if (hostFarm.getWater(waterNorm) != waterNorm)
                health -= maxHealth / 4;
        }
        private void ageUp()
        {
            age += 1;
        }
        private void die()
        {
            hostFarm.animalDied(this);            
        }

    }
}
