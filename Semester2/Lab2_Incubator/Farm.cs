﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace IncubatorCS
{
    class Farm
    {
        private bool fine = false;
        public bool IsFine
        {
            get { return fine; }
        }
        private static Farm instance = null;
        private static readonly object padlock = new object();
        private Farm()
        {
            water = 1000;
            feed = 1000;
        }
        public static Farm Instance
        {
            get
            {
                if(instance == null)
                {
                    lock (padlock)
                    {
                        if (instance == null)
                        {
                            instance = new Farm();
                        }
                    }
                }
                return instance;
            }
        }
        static private List<Animal> animals = new List<Animal>();
        const int maxWater = int.MaxValue;
        const int maxFeed = int.MaxValue;
        const int increasePopulationTime = 10000;
        private int feed;
        private int water;
        public int Feed
        {
            get { return feed; }
        }
        public int Water
        {
            get { return water; }
        }
        public List<Animal> Animals
        {
            get
            { return animals; }
        }
        public async void Begin()
        {
            animals.Add(new Animal());
            animals.Add(new Animal());
            ShowResourcesAsync();
            await DoWork();
        }
        private async void ShowResourcesAsync()
        {
            await ShowResourcesDoWork();
        }
        private Task ShowResourcesDoWork()
        {
            return Task.Run(() =>
            {
                while (!Farm.Instance.IsFine)
                {
                    showInfo();
                    Thread.Sleep(500);
                }
            });
        }
        private Task DoWork()
        {
            return Task.Run(() =>
            {
                while (animals.Count > 1)
                {
                    Thread.Sleep(increasePopulationTime);
                    if ( water > 0 && feed > 0)
                        increasePopulation();
                }
                Console.WriteLine("Game over.");
                fine = true;
            });
        }
        private void increasePopulation()
        {
            lock (padlock)
            {
                int count = animals.Count;
                for (int i = 0; i < count; i++)
                {
                    animals.Add(new Animal());
                }
            }
        }
        public int getFeed(int kg)
        {
            //lock (padlock){
                if (feed - kg >= 0)
                {
                    feed -= kg;
                    return kg;
                }
                else return 0;                
            //}
        }
        public int getWater(int liters)
        {
            //lock (padlock){
                if (water - liters >= 0)
                {
                    water -= liters;
                    return liters;
                }
                else return 0;
            //}
        }
        public void addFeed(int value)
        {
            feed += value;
        }
        public void addWater(int value)
        {
            water += value;
        }
        public void animalDied(Animal animal)
        {
            lock (padlock)
            {
                try
                {
                    animals.Remove(animal);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
        public void showInfo()
        {
            Console.Clear();
            Console.WriteLine("Animals count : {0}", animals.Count);
            Console.WriteLine("Water left : {0}", water);
            Console.WriteLine("Feed left : {0}", feed);
            Console.WriteLine();
            Console.WriteLine();
        }
    }
}
