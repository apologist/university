﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace IncubatorCS
{
    class Program
    {
        static void Main(string[] args)
        {
            const int inc = 1000;
            Console.Title = "Farm Incubator, Lab. No 2";
            Console.CursorSize = 10;
            //Console.CursorVisible = false;
            //Console.BackgroundColor = ConsoleColor.Yellow;
            //Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Welcome to your small farm!");
            Console.WriteLine
                (
                    "Press enter to create your farm.\nEnter ESC to finish game.\nEnter W to add 1000 liters of water.\nEnter F to add 1000 kg of feed.\nHave fun!"
                );
            do
            {
                ConsoleKeyInfo c = Console.ReadKey();
                if (c.Key == ConsoleKey.Enter)
                    break;
            } while (true);
            try
            {
                Farm.Instance.Begin();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            do
            {
                ConsoleKeyInfo c = Console.ReadKey();
                if (c.Key == ConsoleKey.Escape)
                {                    
                    break;
                }
                if (c.Key == ConsoleKey.F)
                {
                    Console.Write("\b");
                    Farm.Instance.addFeed(inc);
                }
                if (c.Key == ConsoleKey.W)
                {
                    Console.Write("\b");
                    Farm.Instance.addWater(inc);
                }
                if (Farm.Instance.IsFine == true)
                {
                    Console.WriteLine("Press any key to finish..");
                    Console.ReadKey();
                    break;                    
                }
            } while (true);
            
        }
    }
}
