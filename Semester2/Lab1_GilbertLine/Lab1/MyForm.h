#pragma once

namespace Lab1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			label1->Text = trackBar1->Value.ToString();
			int sq = 775;
			pictureBox1->Width = sq;
			pictureBox1->Height = sq;
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::PictureBox^  pictureBox1;
	protected:
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::TrackBar^  trackBar1;
	private: System::Windows::Forms::Label^  label1;


	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->trackBar1 = (gcnew System::Windows::Forms::TrackBar());
			this->label1 = (gcnew System::Windows::Forms::Label());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBar1))->BeginInit();
			this->SuspendLayout();
			// 
			// pictureBox1
			// 
			this->pictureBox1->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->pictureBox1->Location = System::Drawing::Point(27, 63);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(500, 500);
			this->pictureBox1->TabIndex = 0;
			this->pictureBox1->TabStop = false;
			// 
			// button1
			// 
			this->button1->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button1->Font = (gcnew System::Drawing::Font(L"Comic Sans MS", 18, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button1->Location = System::Drawing::Point(27, 12);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(247, 45);
			this->button1->TabIndex = 1;
			this->button1->Text = L"Draw Gilbert curve";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MyForm::button1_Click);
			// 
			// trackBar1
			// 
			this->trackBar1->Location = System::Drawing::Point(296, 13);
			this->trackBar1->Maximum = 8;
			this->trackBar1->Minimum = 1;
			this->trackBar1->Name = L"trackBar1";
			this->trackBar1->Size = System::Drawing::Size(180, 45);
			this->trackBar1->TabIndex = 3;
			this->trackBar1->Value = 1;
			this->trackBar1->Scroll += gcnew System::EventHandler(this, &MyForm::trackBar1_Scroll);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Comic Sans MS", 18, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label1->Location = System::Drawing::Point(498, 13);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(26, 33);
			this->label1->TabIndex = 4;
			this->label1->Text = L"1";
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->AutoSize = true;
			this->ClientSize = System::Drawing::Size(547, 575);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->trackBar1);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->pictureBox1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedToolWindow;
			this->Name = L"MyForm";
			this->Text = L"Gilbert curve Lab. No.1";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBar1))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
		
		/// <summary>
		/// Exponent of the cure.
		/// </summary>
		int p = 1; 
		/// <summary>
		/// Pixels between lines.
		/// </summary>
		int px = 5;
		/// <summary>
		/// Pixels between lines.
		/// </summary>
		int lx = px, ly = px;				
		/// <summary>
		/// Position of the draw element at the begining.
		/// </summary>	
		int X = 0, Y = 0;
		int i;
		
		/// <summary>
		/// Draw line from point (X,Y) and save coordinates in X and Y.
		/// </summary>
		private: void DrawPart(Graphics^ g, int lx, int ly)
		{
			g->DrawLine(Pens::Black, X, Y, X + lx, Y + ly);			
			X = X + lx;
			Y = Y + ly;
		}

		/// <summary>
		/// We can get Gilbert's curve with connection of elements a, b, c, d.
		/// Each element is built by specific function.
		/// </summary>

		/// <summary>
		/// Take four small Gilbert's curves and connect them with lines in recurssion.
		/// </summary>
		void a(int i, Graphics^ g)
		{
			if (i > 0)
			{
				d(i - 1, g);
				//�� ��������� ����� ���������� ������ ������� ������ 5 ��������
				//From the last point draw right line of lx pixels.
				DrawPart(g, +lx, 0);
				a(i - 1, g);
				//From the last point draw down line of lx pixels to the right.
				DrawPart(g, 0, ly);
				a(i - 1, g);
				//From the last point draw left line of lx pixels to the right.
				DrawPart(g, -lx, 0);
				c(i - 1, g);
			}
		}

		/// <summary>
		/// Take four small Gilbert's curves and connect them with lines in recurssion.
		/// </summary>
		void b(int i, Graphics^ g)
		{
			if (i > 0)
			{
				c(i - 1, g);
				//From the last point draw left line of lx pixels to the right.
				DrawPart(g, -lx, 0);
				b(i - 1, g);
				//From the last point draw up line of lx pixels to the right.
				DrawPart(g, 0, -ly);
				b(i - 1, g);
				//From the last point draw right line of lx pixels to the right.
				DrawPart(g, lx, 0);
				d(i - 1, g);
			}
		}

		/// <summary>
		/// Take four small Gilbert's curves and connect them with lines in recurssion.
		/// </summary>
		void c(int i, Graphics^ g)
		{
			if (i > 0)
			{

				b(i - 1, g);
				//From the last point draw up line of lx pixels to the right.
				DrawPart(g, 0, -ly);
				c(i - 1, g);
				//From the last point draw left line of lx pixels to the right.  
				DrawPart(g, -lx, 0);
				c(i - 1, g);
				//From the last point draw down line of lx pixels to the right. 
				DrawPart(g, 0, ly);
				a(i - 1, g);
			}
		}

		/// <summary>
		/// Take four small Gilbert's curves and connect them with lines in recurssion.
		/// </summary>
		void d(int i, Graphics^ g)
		{
			if (i > 0)
			{
				a(i - 1, g);
				//From the last point draw down line of lx pixels to the right. 
				DrawPart(g, 0, ly);
				d(i - 1, g);
				//From the last point draw right line of lx pixels to the right. 
				DrawPart(g, lx, 0);
				d(i - 1, g);
				//From the last point draw up line of lx pixels to the right. 
				DrawPart(g, 0, -ly);
				b(i - 1, g);
			}
		}
		private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) 
		{			
			this->lx = this->ly = px;
			this->X = 0;
			this->Y = 0;
			pictureBox1->Refresh();
			//Make a handle of Graphics g to pictureBox1.
			IntPtr hwnd = pictureBox1->Handle;
			Graphics^ g = Graphics::FromHwnd(hwnd);
			//Draw fractal.
			a(p, g);
		}
	private: System::Void trackBar1_Scroll(System::Object^  sender, System::EventArgs^  e) 
	{
		label1->Text = trackBar1->Value.ToString();
		p = trackBar1->Value;
		if (trackBar1->Value == 8)
			px = 3;
		else px = 5;
	}
};
}
